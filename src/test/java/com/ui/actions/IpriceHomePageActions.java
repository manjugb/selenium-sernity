package com.ui.actions;

import com.ui.pages.IpriceHomePage;

import net.thucydides.core.annotations.Step;

public class IpriceHomePageActions {
IpriceHomePage ipricehomepage;

@Step
public void openIpriceHomePageurl()
{
	ipricehomepage.open();

}

@Step
public void enter_text(String key) throws Throwable {
	ipricehomepage.enterText(key);
}

@Step
public void clk_submit() throws Throwable{
	ipricehomepage.clk_search_but();
}

}
