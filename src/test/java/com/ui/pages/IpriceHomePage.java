package com.ui.pages;

import net.serenitybdd.core.pages.PageObject;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.ui.utils.CommonUtils;

import net.serenitybdd.core.annotations.findby.FindBy;
/*
 * 
 */
public class IpriceHomePage extends PageObject{
//Text Search Box
	@FindBy(xpath="//input[@id='term-desktop']")
	private WebElement searchBox;
	//Submit Search Button
	@FindBy(xpath="//i[@class='sprite-icons cI i-search-orange-desk']")
	private WebElement clickSearch;
	//list of Products
	@FindBy(xpath="//div[@class='i5 G']")
	private List<WebElement> listProducts;
	//sort by popularity
	@FindBy(xpath="//span[@class='eq l pl et']")
	private WebElement sortByPopularity;
	//sort by price
	@FindBy(xpath="//a[@class='as ez p qv i7 eF O bO eG eH et b--gray-dark dg']")
	private WebElement sortByPrice;
   //sort by relavence
	@FindBy(xpath="//span[@class='eq l pl tY']")
	private WebElement sortByRelavence;
	//Choose Coupons
	@FindBy(xpath="//body[@id='body']/div[@role='main']/header[@id='main-menu-top']/div[@id='top-nav']/div[@class='dn cO bi mw gN pt1']/a[2]")
	private WebElement clickCouponsTab;
	//Choose Trends
	@FindBy(xpath="//span[@class='M bY em ff'][contains(text(),'Trends')]")
	private WebElement clickTrends;
	
	//Search Key text field
	public void enterText(String key) throws Throwable{
		searchBox.sendKeys(key);
	}
	
	//submit click
	public void clk_search_but() throws Throwable{
		clickSearch.click();
	}
	
	
	
	 public String getIpriceHomePageTitle()
	   {
	      return getDriver().getTitle();
	   }
	 
	 

	   public List<String> getPoductList()
	   {

	      List<String> productList = new ArrayList<String>();

	      int productSize = listProducts.size();
	      for (int i = 0; i < productSize ; i++)
	      {
	         productList.add(listProducts.get(i).getText());
	      }
	      return productList;

	   }
	   
	   public int getProductRowIndexByName(String productName)
	   {
	      for (int i = 0; i < listProducts.size(); i++)
	      {
	         if (listProducts.get(i).getText().equalsIgnoreCase(productName))
	         {
	            return i + 2;
	         }
	      }
	      return 1;
	   }

	   public String getProductInStockQuantityByIndex(int index)
	   {
	      return getDriver().findElement(By.xpath("//tr["+index+"][contains(@class,'line_item')]/td[3]")).getText();
	   }

	   public String getProductPriceByIndex(int index)
	   {

	      return getDriver().findElement(By.xpath("//tr["+index+"][contains(@class,'line_item')]/td[2]")).getText();
	   }
	   

}
