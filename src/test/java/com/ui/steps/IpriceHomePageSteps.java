package com.ui.steps;

import com.ui.actions.IpriceHomePageActions;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import net.thucydides.core.annotations.Steps;

public class IpriceHomePageSteps {

	@Steps
	IpriceHomePageActions ipricehompageactions;
	
	
	@Given("I open the iprice home page url")
	public void I_open_the_iprice_home_page_url() {
		ipricehompageactions.openIpriceHomePageurl();
	}
	
	@Then("Enter valid Search key {string}")
	public void enter_valid_sea_key(String key) throws Throwable {
		ipricehompageactions.enter_text(key);
	}
	
	@Then("I Click on Search Button")
	public void clk_search_but() throws Throwable {
		ipricehompageactions.clk_submit();
	}
	
}
